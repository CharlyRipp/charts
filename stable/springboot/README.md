# SpringBoot

[SpringBoot](http://spring.io/projects/spring-boot) makes it easy to create stand-alone, production-grade Spring based Applications that you can "just run".

## TL;DR;

```bash
$ helm repo add io-determan https://io-determan-charts.storage.googleapis.com
$ helm install io-determan/springboot
```

## Introduction

This chart bootstraps a [SpringBoot](http://spring.io/projects/spring-boot) deployment on a [Kubernetes](http://kubernetes.io) cluster using the [Helm](https://helm.sh) package manager.

This is a generic chart for quick deployment of SpringBoot applications.

## Prerequisites

- Kubernetes 1.8+

## Installing the Chart

To install the chart with the release name `my-release`:

```bash
$ helm install --name my-release io-determan/springboot
```

The command deploys SpringBoot on the Kubernetes cluster in the default configuration. The [configuration](#configuration) section lists the parameters that can be configured during installation.

> **Tip**: List all releases using `helm list`

## Uninstalling the Chart

To uninstall/delete the `my-release` deployment:

```bash
$ helm delete my-release
```

The command removes all the Kubernetes components associated with the chart and deletes the release.

## Configuration

The following table lists the configurable parameters of the SpringBoot chart and their default values.

| Parameter                            | Description                                      | Default                                                 |
| ------------------------------------ | ------------------------------------------------ | ------------------------------------------------------- |
| `global.imageRegistry`               | Global Docker image registry                     | `nil`                                                   |
| `image.registry`                     | Rabbitmq Image registry                          | `docker.io`                                             |
| `image.repository`                   | Rabbitmq Image name                              | `""`                                                    |
| `image.tag`                          | Rabbitmq Image tag                               | `latest`                                                |
| `image.pullPolicy`                   | Image pull policy                                | `Always` if `imageTag` is `latest`, else `IfNotPresent` |
| `image.pullSecrets`                  | Specify docker-registry secret names as an array | []     ß                                                 |
| `resources`                          | resource needs and limits to apply to the pod    | {}                                                      |
| `nodeSelector`                       | Node labels for pod assignment                   | {}                                                      |
| `affinity`                           | Affinity settings for pod assignment             | {}                                                      |
| `tolerations`                        | Toleration labels for pod assignment             | []                                                      |
| `ingress.enabled`                    | Enable ingress resource for Management console   | `false`                                                 |
| `ingress.hostName`                   | Hostname to your RabbitMQ installation           | `nil`                                                   |
| `ingress.paths`                      | Paths within the url structure                   | []                                                      |
| `ingress.tls`                        | enable ingress with tls                          | `false`                                                 |
| `ingress.tlsSecret`                  | tls type secret to be used                       | `myTlsSecret`                                           |
| `ingress.annotations`                | ingress annotations as an array                  | []                                                      |
| `livenessProbe.enabled`              | would you like a livenessProbed to be enabled    | `true`                                                  |
| `livenessProbe.path`                 | health check path                                | `/actuator/health`                                      |
| `livenessProbe.initialDelaySeconds`  | number of seconds                                | 120                                                     |
| `livenessProbe.timeoutSeconds`       | number of seconds                                | 20                                                      |
| `livenessProbe.periodSeconds`        | number of seconds                                | 30                                                      |
| `livenessProbe.failureThreshold`     | number of failures                               | 6                                                       |
| `livenessProbe.successThreshold`     | number of successes                              | 1                                                       |
| `readinessProbe.enabled`             | would you like a readinessProbe to be enabled    | `true`                                                  |
| `readinessProbe.path`                | health check path                                | `/actuator/health`                                      |
| `readinessProbe.initialDelaySeconds` | number of seconds                                | 10                                                      |
| `readinessProbe.timeoutSeconds`      | number of seconds                                | 20                                                      |
| `readinessProbe.periodSeconds`       | number of seconds                                | 30                                                      |
| `readinessProbe.failureThreshold`    | number of failures                               | 3                                                       |
| `readinessProbe.successThreshold`    | number of successes                              | 1                                                       |